// import "./styles.css";
// // Step 1 : "Hello, Heroku ! 👋"
// fetch("http://localhost:3010")
//     .then((res) => res.json())
//     .then((res) => console.log(res));
// Step 2 : "Get JWT token 🔓"
fetch("http://localhost:3010/register", {
    method: "POST",
    body: JSON.stringify({
        username: "Bob",
        password: "utilisateur"
    }),
    headers: {
        "Content-type": "application/json"
    }
}).then((res)=>res.json()).then((res)=>{
    return res.token;
}).then((token)=>fetchUserlist(token));
let userdatas = "Bienvenue";
// Step 3 : "Get user list 🎉"
const fetchUserlist = (token)=>{
    fetch("http://localhost:3010/users", {
        headers: {
            Authorization: `Bearer ${token}`
        }
    }).then((res)=>res.json()).then((res)=>{
        res.data.forEach((element)=>{
            console.log(element);
        });
    });
};
module.exports = fetchUserlist();

//# sourceMappingURL=index.579125c3.js.map

import { Routes, Route } from "react-router-dom"
import UsersComponent from "./Components/UsersComponent"


function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/users" element={ <UsersComponent/>}/>
      </Routes>
    </div>
  );
}

export default App;

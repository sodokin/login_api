import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux";
import { getUsers } from "../redux/theReducer/userReducer";
import { v4 as uuidv4 } from "uuid"


export default function UsersComponent() {
    const {  users } = useSelector(state => ({
        ...state.userReducer
    }))
    const dispatch = useDispatch();

    useEffect(() => {
        if(users.length === 0) {
            dispatch(getUsers());
        }
    }, [dispatch, users.length])
    console.log(users)

    return (
        <>
            
            <ul>
                {users.map(user => {
                    return (
                        <h2 key={uuidv4()}>
                            <li> {user.username} </li>
                        </h2>
                        )
                    })}
            </ul>
        
        </>
    )
}

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const user = require('../models/mongoUserModel')

// Ici il s'agit du module de gestion de la connexion à la base de données ainsi que l'enregistrement des utilisateurs
// On utilise bcrypt afin d'encrypter les différents mots de passe enregistrés

module.exports = (mongooseConnect) => {
    mongoose.connect('mongodb+srv://Cluster86080:utilisateur@cluster86080.8zevvmt.mongodb.net/?retryWrites=true&w=majority')
        .then(_ => { bcrypt.hash('utilisateur', 10)
                        .then(hash => {
                            user.create({ 
                                username: "Bob@gmail.com", 
                                password: hash})
                        })
        })
        .then(() => { console.log('The database is connected') })
        .catch(() => { console.log('Connection to the database failed!') })
}
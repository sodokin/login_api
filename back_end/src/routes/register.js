const user = require('../models/mongoUserModel')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const privateKey = require('../auth/private_key');

// Ici est le point de terminaison permettant à un futur utilisateur de s'enregistrer
module.exports = (app) => {
    app.post('/register', (req, res) => {
        user.findOne({ where: { username: req.body.username} }).then(user => {
            if(!user) {
                const message = `This user not exist!`
                return res.status(404).send({message})
            }
            bcrypt.compare(req.body.password, user.password).then(isPasswordValid => {
                if(!isPasswordValid) {
                    const message = `Incorrect password`;
                    return res.status(401).json({  message });
                }
                // Zone du jsonwebtoken
                const token = jwt.sign(
                    {userId: user.id},
                    privateKey,
                    { expiresIn: '24h'}
                )

                const message = `The user has successfully logged in`;
                return res.json({  message, data: user, token });
            })
        })
        .catch(error => {
            const message = `The user has not logged in, Please try again later`;
            return res.json({  message, data: error });
        })
    })
};
const user = require('../models/mongoUserModel')
const auth = require('../auth/auth') // Ici on importe le middleware qui permet de sécuriser les échanges entre les utilisateurs de l'application et l'api

// Ici il s'agit du point de terminaison permettant l'affichage des utilisateurs et leur connexion
module.exports = (app) => {
    app.get('/login',auth, (req, res) => { // On ajoute le middleware de sécurisation auth
        user.find() // Ici on récupère la liste des utilisateurs puis on gère soit le cas de succès soit le cas d'échec de la requête
            .then(user => { 
                const message = "The list of users has been successfully retrieved"
                res.json({ message, data: user })
            })
            .catch(error => {
                const message = "An error has occurred while"
                res.status(500).json({ message, data: error })
            })
    }
)}

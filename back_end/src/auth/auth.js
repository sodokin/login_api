// Ce fichier correspond à un middleware qui servira à sécuriser les échanges entre les utilisateurs de l'application et l'api
const jwt = require('jsonwebtoken');
const privatekey = require('../auth/private_key')

module.exports = (req, res, next) => {
    // Ici on récupère l'en-tête http 'authorization' qui est l'en-tête par laquelle transitera le jeton envoyé par les utilisateurs
    const authorizationHeader = req.headers.authorization
    //  Ici on vérifie que le jeton a bien été fourni
    if(!authorizationHeader) {
        const message = `You did not provide an authentication token. Add one in the request header`
        return res.status(401).json({ message })
    }
    // Ici on récupère le jeton envoyé
        const token = authorizationHeader.split(' ')[1]
        const decodedToken = jwt.verify(token, privatekey, (error,decodedToken) => { // Ici grace à la methode 'verify' on vérifie si le jeton est bien valide
            if(error) {
                const message = `The user is not allowed to access this resource`
                return res.status(401).json({ message, data: error })
            }
            // Ici on refuse l'accès à l'utilisateur si le jeton ne correspond pas
            const userId = decodedToken.userId
            if(req.body.userId && req.body.userId !== userId) {
                const message = `the identifier is not valid`
                return res.status(401).json({ message })
            } else {
                next() // Dans la mesure ou tous les tests sont passé avec succès , on autorise l'accès à l'utilisateur avec la methode next()
            }
        })
}
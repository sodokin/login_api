//Ici se trouve le modèle permettant de créer la collection dans la base de données

const mongoose = require('mongoose');

const user = mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model("User", user)
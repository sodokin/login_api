const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongooseConnect = require('./src/db/mongooseconfig')
const cors = require('cors');

const app = express();
const port = 3010

// Ici sont listés les middlewares utilisés dans l'api
app
    .use(bodyParser.json())
    .use(morgan("dev"))
    .use(cors())
    
// Ceci est la fonction de connexion à la base de données
mongooseConnect()
// Ici j'indique les diférentes routes que comportent l'api 
//  /register designe la route d'enregitrement d'un utilisateur
require('./src/routes/register')(app);
//  /login désigne la route de connexion de l'utilisateur enregistré
require('./src/routes/login')(app);

// Ceci est un middleware personnalisé permettant de gérer les erreurs qui surviennent lorsqu'un utilisateur appelle un mauvais point de terminaison
app.use(({res}) => {
    const message = "Unable to find the requested resource! You can try another URL."
    res.status(404).json({message})
    }
    )
    

app.listen(port, () => console.log(`The application is listening on ${port}`));
